package com.novo.tech.redmango;


import android.util.Log;
import com.novo.tech.redmango.utils.SunmiPrintHelper;

import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.Serializable;
import java.util.Objects;


public class CashMemoHelper {
    public static void printCustomerMemo(JSONObject data){
        String header = data.optString("storeName")+"\n";
        header+=data.optString("location");
        header+="\n............................\n";
        String str1=data.optString("orderType") +"  Order #"+data.optString("orderId");
        str1+="\nPrinted "+data.optString("printDate");
        str1+="\nPayment Method : "+data.optString("method");
        str1+="\n........................\n";

        SunmiPrintHelper.getInstance().printText(header, 28, true, false,1);
        SunmiPrintHelper.getInstance().printText(str1, 28, false, false,1);
        JSONArray items = data.optJSONArray("items");
        for(int i = 0; i< Objects.requireNonNull(items).length(); i++){
            String str3= "\n"+String.valueOf(i+1)+" "+items.optJSONObject(i).optString("name")+"\n"+items.optJSONObject(i).optString("components");
            String str4="             "+items.optJSONObject(i).optString("quantity")+" X "+items.optJSONObject(i).optString("price")+" = "+items.optJSONObject(i).optString("total");
            String str5 = "\nNote : "+items.optJSONObject(i).optString("note")+"\n";
            SunmiPrintHelper.getInstance().printText(str3, 28, false, false,0);
            SunmiPrintHelper.getInstance().printText(str4, 25, false, false,0);
            SunmiPrintHelper.getInstance().printText(str5, 25, false, false,0);
        }
        String summery = "\n...........................";

        summery+="\nDiscount     "+data.optString("discount");
        summery+="\nSubtotal     "+data.optString("subtotal");
        if(data.optString("orderType").equals("DELIVERY")){
            summery+="\nDelivery Charge        "+data.optString("deliveryCharge");
        }
        summery+="\nTotal     "+data.optString("total")+"\n";

        String dlAddress="........................\n";

        dlAddress+=data.optString("orderType")+" Time : "+data.optString("deliveryTime")+"\n";
        dlAddress+="Requested : "+data.optString("actualDeliveryTime");
        JSONObject customer = data.optJSONObject("customer");


        if(customer != null){
                dlAddress+="\nName : "+customer.optString("name");

                dlAddress+="\nAddress : "+customer.optString("address");

                dlAddress+="\nPhone : "+customer.optString("phone");
        }
        String comment = "\nComments : "+ data.optString("comments")+"\n \n";
        SunmiPrintHelper.getInstance().printText(summery, 28, false, false,2);
        SunmiPrintHelper.getInstance().printText(dlAddress, 28, false, false,0);
        SunmiPrintHelper.getInstance().printText(comment, 28, false, false,0);
        SunmiPrintHelper.getInstance().feedPaper();
        SunmiPrintHelper.getInstance().cutpaper();
    }


}
