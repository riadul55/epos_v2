package com.novo.tech.redmango;

import android.util.Log;

import io.flutter.embedding.android.FlutterActivity;

import androidx.annotation.NonNull;

import com.novo.tech.redmango.utils.SunmiPrintHelper;

import org.json.JSONObject;

import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.plugin.common.MethodChannel;

public class MainActivity extends FlutterActivity {
    private static final String CHANNEL = "printerHelper";

    @Override
    public void configureFlutterEngine(@NonNull FlutterEngine flutterEngine) {
        super.configureFlutterEngine(flutterEngine);
        new MethodChannel(flutterEngine.getDartExecutor().getBinaryMessenger(), CHANNEL)
                .setMethodCallHandler(
                        (call, result) -> {
                            if (call.method.equals("printData")) {
                                SunmiPrintHelper.getInstance().initSunmiPrinterService(getApplicationContext());
                                String printingData = call.arguments.toString();
                                byte[] dataByte = (byte[]) call.arguments();
                                SunmiPrintHelper.getInstance().sendRawData(dataByte);
                                SunmiPrintHelper.getInstance().feedPaper();
                                SunmiPrintHelper.getInstance().cutpaper();

                            } else {
                                result.notImplemented();
                            }
                        }
                );
    }
}
