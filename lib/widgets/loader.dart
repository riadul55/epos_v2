import 'package:flutter/material.dart';
import 'package:redmango/extra/apps_data.dart';

class LoaderDialog{
  static show(context,{String? loadingText}){

    showDialog(
        context: context,
        barrierDismissible: false,
        barrierColor: Colors.black.withOpacity(.25),
        builder: (ctx)=>AlertDialog(
          scrollable: true,
          elevation: 5,
          content: Column(
            children: [
              Container(
                  margin: EdgeInsets.only(bottom: 10),
                  child: Text(
                    loadingText??"Loading",
                    style: TextStyle(
                        fontWeight: FontWeight.w600
                    ),
                  )
              ),
              LinearProgressIndicator(
                valueColor: AlwaysStoppedAnimation(mainColor),
                backgroundColor: mainColor.withOpacity(.5),

              )
            ],
          ),
        )
    );
  }
  static hide(context){
    Navigator.pop(context);
  }
}
