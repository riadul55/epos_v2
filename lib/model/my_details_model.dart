import 'address.dart';

class CustomerModel{
    String? consumerUuid,email,username;
    ProfileModel? profile;
    List<Address> addresses = [];
    CustomerModel(
        {this.consumerUuid,
            this.email,
            this.username,
            this.profile,
            required this.addresses,
        });
    factory CustomerModel.fromJSON(Map<String,dynamic> data){
      List<Address> _address = [];
      if(data["addresses"] != null)
      data["addresses"].forEach((e){
        _address.add(Address.fromData(e));
      });
      return CustomerModel(
        consumerUuid: data["consumer_uuid"],
        email: data["email"],
        username: data["username"],
        profile:data["profile"]==null?ProfileModel(firstName: "N/A",lastName: "N/A",phoneNumber: "N/A") : ProfileModel.fromJSON(data["profile"]),
        addresses: _address
      );
    }
    static Map<String,dynamic> toJSON(CustomerModel model){
      return {
        "consumer_uuid":model.consumerUuid,
        "email":model.email,
        "username":model.username,
        "profile": ProfileModel.toJSON(model.profile!),
        "addresses":model.addresses.map((e) => Address.toJSON(e)).toList(),
      };
    }


}
class ProfileModel{
  String? firstName, lastName, phoneNumber;
  ProfileModel({this.firstName,this.lastName,this.phoneNumber});
  factory ProfileModel.fromJSON(Map<String,dynamic> data){
    return ProfileModel(
      firstName: data["first_name"],
      lastName: data["last_name"],
      phoneNumber: data["phone"],
    );
  }
  static Map<String,dynamic> toJSON(ProfileModel model){
    return {
      "first_name":model.firstName,
      "last_name":model.lastName,
      "phone":model.phoneNumber,
    };
  }
}