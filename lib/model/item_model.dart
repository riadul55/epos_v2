import 'package:redmango/extra/dateService.dart';

class ItemModel{
  String uuid,providerUuid,status,description,shortName;
  List<ComponentModel> components;
  List<ItemsFile> files;
  PriceModel price;
  List<PriceModel> priceHistory;
  ProductProperties properties;
  DateTime creationDate;
  bool visible;
  double avgRate;
  ItemModel({
      required this.uuid,
      required this.providerUuid,
      required this.status,
      required this.description,
      required this.shortName,
      required this.components,
      required this.files,
      required this.price,
      required this.priceHistory,
      required this.properties,
      required this.creationDate,
      required this.visible,
      required this.avgRate
  });
  factory ItemModel.fromJSON(Map<String,dynamic> data){
    List<ComponentModel> components = [];
    if(data["components"] != null){
      data["components"].forEach((element) {
        components.add(ComponentModel.fromJSON(element));
      });
    }
    List<ItemsFile> itemFiles = [];
    if(data["files"] != null){
      data["files"].forEach((e){
        itemFiles.add(ItemsFile.fromJSON(e));
      });
    }
    return ItemModel(
      uuid: data["product_uuid"],
      shortName: data["short_name"],
      description: data["description"],
      status: data["status"],
      price: data["price"]==null?PriceModel(price: 0,priceUuid: "-1",startDate: DateTime.now(), endDate: DateTime.now()):PriceModel.fromJSON(data["price"]),
      priceHistory:data["price_history"]==null?<PriceModel>[]:data["price_history"].forEach((e) => PriceModel.fromJSON(e)),
      avgRate: data["average_rate"],
      providerUuid: data["creator_provider_uuid"],
      visible: data["visible"],
      creationDate:DateFormatHelper.parseDate(data["creation_date"]),
      files:itemFiles,
      properties: ProductProperties.fromJSON(data["properties"]),
      components: components
    );
  }

  Map<String,dynamic> toJSON(ItemModel itemModel){
    return {
      "product_uuid": itemModel.uuid,
      "short_name":itemModel.shortName,
      "description":itemModel.description,
      "status":itemModel.status,
      "price":PriceModel.toJSON(itemModel.price),
      "average_rate":itemModel.avgRate,
      "creator_provider_uuid":itemModel.providerUuid,
      "visible":itemModel.visible,
      "creation_date":itemModel.creationDate,
      "properties":ProductProperties.toJSON(itemModel.properties),
      "components": itemModel.components.map((e) => ComponentModel.toJSON(e)).toList(),
      "files": itemModel.files.map((e) => ItemsFile.toJSON(e)).toList(),
    };
  }


}
class ComponentModel{
    String type;
    String uuid,shortName,description,string,providerId;
    PriceModel price;
    double avgRating;
    bool visible;
    DateTime creationTime;
    ProductProperties properties;
    String relationType,relationGroup;
    List<ItemsFile> files;
    ComponentModel({
      required this.type,
      required this.uuid,
      required this.shortName,
      required this.description,
      required this.string,
      required this.providerId,
      required this.price,
      required this.avgRating,
      required this.visible,
      required this.creationTime,
      required this.properties,
      required this.relationType,
      required this.relationGroup,
      required this.files
    });
    factory ComponentModel.fromJSON(Map<String,dynamic> data){
      List<ItemsFile> componentFiles = [];
      if(data["files"] != null){
        data["files"].forEach((e){
          componentFiles.add(ItemsFile.fromJSON(e));
        });
      }
      return ComponentModel(
         type: data["product_type"],
         uuid: data["product_uuid"],
         shortName: data["short_name"],
         description: data["description"]??"",
         price: data["price"]==null?PriceModel(price: 0,priceUuid: "-1",startDate: DateTime.now(), endDate: DateTime.now()):PriceModel.fromJSON(data["price"]),
         avgRating: data["average_rate"],
         providerId: data["creator_provider_uuid"],
         visible: data["visible"],
         creationTime: DateFormatHelper.parseDate(data["creation_date"]),
         properties: ProductProperties.fromJSON(data["properties"]),
         relationGroup: data["relation_group"]??"",
         relationType: data["relation_type"]??"",
          files: componentFiles,
          string: ''
       );
    }
    static Map<String,dynamic> toJSON(ComponentModel componentModel){
      return {
        "product_type":componentModel.type,
        "product_uuid": componentModel.uuid,
        "short_name":componentModel.shortName,
        "description":componentModel.description,
        "price":PriceModel.toJSON(componentModel.price),
        "average_rate":componentModel.avgRating,
        "creator_provider_uuid":componentModel.providerId,
        "visible":componentModel.visible,
        "creation_date":componentModel.creationTime.toString(),
        "properties":ProductProperties.toJSON(componentModel.properties),
        "relation_group":componentModel.relationGroup,
        "relation_type":componentModel.relationType,
        "files": componentModel.files.map((e) => ItemsFile.toJSON(e)).toList(),
      };
    }
}

class PriceModel {
  String priceUuid;
  double price;
  DateTime startDate;
  DateTime? endDate;
  PriceModel({ this.endDate, required this.price, required this.priceUuid,required this.startDate});
  factory PriceModel.fromJSON(Map<String,dynamic> data){
    return PriceModel(
      priceUuid: data["price_uuid"],
      price: data["price"]??0,
      startDate: DateFormatHelper.parseDate(data["start_date"]),
      endDate: data["end_date"]==null?null:DateFormatHelper.parseDate(data["end_date"]),
    );
  }
  static Map<String,dynamic> toJSON(PriceModel item){
    return {
      "price_uuid":item.priceUuid,
      "price": item.price,
      "start_date":item.startDate.toString(),
      "end_date":item.endDate.toString()
    };
  }
}


class ItemsFile{
  String fileUid, keyName, itemId;
  ItemsFile({required this.fileUid, required this.keyName, required this.itemId});
  factory ItemsFile.fromJSON(Map<String,dynamic> data){
    return ItemsFile(
      fileUid: data["file_uuid"],
      keyName: data["key_name"],
      itemId: data["product_uuid"]
    );
  }
  static Map<String,dynamic> toJSON(ItemsFile item){
    return {
      "file_uuid":item.fileUid,
      "key_name": item.keyName,
      "product_uuid":item.itemId
    };
  }
}
class ProductProperties{
  bool D=false,V=false,G=false,N=false,VE=false;
  String category="",spiced="";

  ProductProperties(bool d, bool v, bool g, bool n, bool VE, String category, String spiced) {
    D = d;
    V = v;
    G = g;
    N = n;
    this.VE = VE;
    this.category = category;
    this.spiced = spiced;
  }
  factory ProductProperties.fromJSON(data){
    return new ProductProperties(
        data["D"]??false,
        data["V"]??false,
        data["G"]??false,
        data["N"]??false,
        data["VE"]??false,
        data["categories"]??"",
        data["spiced"]??"",

    );
  }
  static Map<String,dynamic> toJSON(ProductProperties data){
    return {
      "D":data.D,
      "V":data.V,
      "G":data.G,
      "N":data.N,
      "VE":data.VE,
      "categories":data.category,
      "spiced":data.spiced
    };
  }

}