class AddressProperties {
  var streetName;
  var streetNumber;
  var building;
  var zipCode;
  var country;
  var city;
  var state;

  AddressProperties(
      {this.streetName,
        this.streetNumber,
        this.building,
        this.zipCode,
        this.country,
        this.city,
        this.state});
  factory AddressProperties.fromJSON(Map<String,dynamic> data){
    return AddressProperties(
        country: data['country']??"",
        city: data['city']??"",
        zipCode: data['zip']??"",
        streetNumber: data['street_number']??"",
        state: data['state']??"",
        building: data['building']??"",
        streetName: data['street_name']??"");
  }
  static Map<String,dynamic> toJSON(AddressProperties model){
    return {
      "country":model.country,
      "city":model.city,
      "zip":model.zipCode,
      "street_number":model.streetNumber,
      "state":model.state,
      "building":model.building,
      "street_name":model.streetName,
    };
  }
}
