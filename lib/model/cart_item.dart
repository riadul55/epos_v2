import 'package:redmango/model/item_component_section.dart';

import 'item_model.dart';

class CartItemModel{
  String uuid,description,type,shortName,comment;
  List<ItemComponentSection> componentSections;
  double price;
  double subTotal,total;
  List<ItemsFile> files;
  int quantity;
  var printOrder;
  bool offered;
  CartItemModel({
    required this.uuid,
    required this.description,
    required this.type,
    required this.shortName,
    required this.componentSections,
    required this.price,
    required this.quantity,
    required this.subTotal,
    required this.total,
    required this.files,
    required this.comment,
    required this.offered,
    required this.printOrder,

  });
  factory CartItemModel.fromJSON(Map<String,dynamic> data){
    double _subTotal =0;
    double total=data["net_amount"];
    dynamic printOrder = 0;
    List<ItemComponentSection> componentSections = [];
    if(data["items"] != null){
      data["items"].forEach((element) {
        componentSections.add(ItemComponentSection.fromJSON(element));
      });
    }
    List<ItemsFile> files = [];
    if(data["files"] != null){
      data["files"].forEach((e){
        files.add(ItemsFile.fromJSON(e));
      });
    }
    for (ItemComponentSection section in componentSections){
      total+= section.selectedItem.price.price;
    }
    String productName = "N/A";
    String description = "";
    double price = 0;
    if(data["product_type"] == "DYNAMIC"){
      productName = data["product_short_name"]??data["product_short_name"]??"N/A";
      price = data["net_amount"];
    }else{
      productName = data["product"]["short_name"]??"N/A";
      description = data["product"]["description"]??"";
      try{
        price = data["price"]??data["product"]["price"]==null?0:data["product"]["price"]["price"]??0;
      }catch(e){
        print(data);
      }
    }
    if(data["properties"] != null){
      printOrder = data["properties"]["print_order"]??0;
    }

    print(productName);

    CartItemModel item = CartItemModel(
        uuid: data["product_uuid"],
        type: data["product_type"]??"",
        shortName: productName,
        description: description,
        price: price,
        quantity:data["units"],
        subTotal:data["net_amount"],
        componentSections: componentSections,
        files: files,
        total: total,
        comment: data["comment"]??"",
        offered: data["offered"]??false,
        printOrder: printOrder

    );
    return item;
  }

  static Map<String,dynamic> toJSON(CartItemModel itemModel){
    return {
      "product_uuid": itemModel.uuid,
      "product_type": itemModel.type,
      "product_short_name": itemModel.shortName,
      "product":{
        "short_name":itemModel.shortName,
        "description":itemModel.description,
        "price":itemModel.price,
      },
      "items": itemModel.componentSections.map((e) => ItemComponentSection.toJSON(e)).toList(),
      "net_amount": itemModel.total,
      "units": itemModel.quantity,
      "comment": itemModel.comment,
      "offered": itemModel.offered,
      "files": itemModel.files.map((e) => ItemsFile.toJSON(e)).toList(),
      "properties":{
          "print_order" : itemModel.printOrder
      }
  };
  }
}
class CartComponentModel{
  String uuid,shortName,description,string="",componentImage;
  PriceModel price;
  String relationType,relationGroup;
  CartComponentModel({
    required this.uuid,
    required this.shortName,
    required this.description,
    required this.string,
    required this.price,
    required this.relationType,
    required this.componentImage,
    required this.relationGroup});
  factory CartComponentModel.fromJSON(Map<String,dynamic> data){
    return CartComponentModel(
      uuid: data["product_uuid"],
      shortName: data["short_name"],
      description: data["description"],
      price: data["price"]??0,
      relationGroup: data["relation_group"],
      relationType: data["relation_type"],
      componentImage: data["component_image"],
      string: '',
    );
  }
  Map<String,dynamic> toJSON(CartComponentModel componentModel){
    return {
      "product_uuid": componentModel.uuid,
      "short_name":componentModel.shortName,
      "description":componentModel.description,
      "price":PriceModel.toJSON(componentModel.price),
      "relation_group":componentModel.relationGroup,
      "relation_type":componentModel.relationType,
      "component_image":componentModel.componentImage
    };
  }

}