import 'item_model.dart';

class ItemComponentSection{
  String sectionName,sectionValue;
  int sortingOrder;
  List<ComponentModel> components;
  ComponentModel selectedItem;
  ItemComponentSection({required this.sectionName,required this.components,required this.sectionValue,required this.selectedItem,required this.sortingOrder});


  factory ItemComponentSection.fromJSON(Map<String,dynamic> data){

    return ItemComponentSection(
        sectionName: data["relation_group"],
        sectionValue: data["relation_group"],
        selectedItem: ComponentModel.fromJSON(data["product"]),
        components: [],
        sortingOrder: 0,
    );
  }

  static Map<String,dynamic> toJSON(ItemComponentSection itemModel){
    return {
      "relation_group": itemModel.sectionName,
      "sectionValue":itemModel.sectionValue,
      "product": ComponentModel.toJSON(itemModel.selectedItem),
    };
  }
}
class SortComponentSectionModel{
  int order;
  String name;
  SortComponentSectionModel({required this.name,required this.order});
  factory SortComponentSectionModel.fromJSON(Map<String,dynamic> data){

    return SortComponentSectionModel(
      order: data["sort_order"],
      name: data["relation_group"],
    );
  }

  Map<String,dynamic> toJSON(SortComponentSectionModel itemModel){
    return {
      "sort_order": itemModel.order,
      "relation_group":itemModel.name,
    };
  }
}