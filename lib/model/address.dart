import 'package:redmango/model/address_properties.dart';

class Address {
  var addressUuid;
  var name;
  var type;
  AddressProperties properties;

  Address(
      {this.addressUuid,
      this.name,
      this.type,
      required this.properties});
  factory Address.fromData(Map<String,dynamic> data){
    return Address(
        addressUuid: data['address_uuid'],
        name: data['name'],
        type: data['type'],
        properties: AddressProperties.fromJSON(data["properties"]),
    );
  }
  static Map<String,dynamic> toJSON(Address model){
    return {
      "address_uuid":model.addressUuid,
      "name":model.name,
      "type":model.type,
      "properties":AddressProperties.toJSON(model.properties),
    };
  }
}