class FontSettingModel{
    int header1,header2,header3,header4;
    bool itemBold;

    FontSettingModel({
      required this.header1,
      required this.header2,
      required this.header3,
      required this.header4,
      required this.itemBold
    });

    factory FontSettingModel.fromJSON(data){

      return FontSettingModel(
          header1: data["header1"],
          header2: data["header2"],
          header3: data["header3"],
          header4: data["header4"],
          itemBold: data["itemBold"]
      );
    }

    Map<String,dynamic> toJSON(){
      return {
        "header1":header1,
        "header2":header2,
        "header3":header3,
        "header4":header4,
        "itemBold":itemBold
      };
    }
}