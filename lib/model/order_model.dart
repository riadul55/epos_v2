 import 'package:intl/intl.dart';
import 'package:redmango/model/address.dart';
import 'package:redmango/model/cart_item.dart';
import 'package:redmango/model/my_details_model.dart';

class OrderModel {
   int? discountPercentage;
   int orderId;
   String? paymentUid,adjustmentNote,orderChannel,deliveryType,orderType,paymentMethod,discountCode;
   String orderStatus,paymentStatus="UNPAID",comments="No Note";
   double? subTotal,tips,receive,refundAmount,adjustmentAmount,plasticBag,containerCost;
   double total=0,deliveryCharge=0,discountAmount=0;
   DateTime? orderDate,orderDateTime,currentDeliveryTime,requestedDeliveryTime;
   List<CartItemModel> items = [];
   CustomerModel? customer;

   OrderModel( String paymentMethod, double subTotal, double total, double tips, List<CartItemModel> items,DateTime orderDateTime,DateTime currentDeliveryTime,
      DateTime requestedDeliveryTime, String deliveryType,String paymentStatus,
      double receive, DateTime orderDate,String comments,
      String paymentUid,double deliveryCharge,
      int discountPercentage,double refundAmount,double adjustmentAmount,String adjustmentNote,{this.customer,required this.orderId,required this.orderChannel,required this.orderType,required this.orderStatus,required this.discountAmount,this.discountCode,this.plasticBag,this.containerCost}) {
    this.paymentMethod = paymentMethod;
    this.subTotal = subTotal;
    this.total = total;
    this.tips = tips;
    this.items = items;
    this.orderDateTime = orderDateTime;
    this.orderDate = orderDate;
    this.currentDeliveryTime = currentDeliveryTime;
    this.requestedDeliveryTime = requestedDeliveryTime;
    this.deliveryType = deliveryType;
    this.paymentStatus = paymentStatus;
    this.receive = receive;
    this.comments = comments;
    this.paymentUid = paymentUid;
    this.deliveryCharge = deliveryCharge;
    this.discountPercentage = discountPercentage;
    this.refundAmount = refundAmount;
    this.adjustmentAmount =adjustmentAmount;
    this.adjustmentNote = adjustmentNote;

  }
  factory OrderModel.fromJSON(data) {
  double _subTotal = 0;
  double _total = 0;
  double _tips = 0;
  double plasticBag = 0;
  double containerCost = 0;
  double _deliveryCharge = data["delivery_charges"]??0;
  String discountCode=data["discount_code"]??"";
  List<CartItemModel> _items = [];
  var orderProperties = data["order_properties"];
  if(data["order_items"]!= null){
    data["order_items"].forEach((jsonItem){
      CartItemModel _itm = CartItemModel.fromJSON(jsonItem);
      if(_itm.type == "DELIVERY"){
        try{
          _deliveryCharge = _itm.total;
        }catch(e){

        }
      }else if(_itm.type == "DISCOUNT"){
        discountCode = _itm.description;
      }else{
        _subTotal+= (_itm.total);
        if (_itm.uuid == "plastic-bag") {
          plasticBag = _itm.quantity * _itm.price;
        } else if (_itm.uuid == "container") {
          containerCost = _itm.quantity * _itm.price;
        }else{
          if(_itm.type=="DYNAMIC"){
            _itm.offered = true;
            if(orderProperties != null){
              _itm.printOrder = orderProperties["print_"+_itm.uuid]??0;
            }

          }
          _items.add(_itm);
        }

      }

    });
  }
  _total = _subTotal;
  _total+=_deliveryCharge;
  String comments = "No Note Found";
  if(data["order_properties"]!=null){
    comments = data["order_properties"]["comment"]??"";
  }
  CustomerModel customerModel = CustomerModel(addresses: []);
  if(data["requester"]!=null){
    var info = data["requester"];
    customerModel = CustomerModel.fromJSON(info);
  }
  if(data["shipping_address"]!=null){
    customerModel.addresses = [];
    customerModel.addresses.insert(0, Address.fromData(data["shipping_address"]));
  }
  var _format = DateFormat("yyyy-MM-dd HH:mm:ss");

  String requestedDeliveryTime =  data["requested_delivery_timestamp"];
  if(data["current_delivery_timestamp"] == null) {
    data["current_delivery_timestamp"] = requestedDeliveryTime;
  }


  return new OrderModel(
  data["payment_type"], _subTotal, _total,
  _tips,
  _items,
  _format.parse(data["creation_date"],true).toLocal(),
      _format.parse(data["current_delivery_timestamp"],true).toLocal(),
    _format.parse(data["requested_delivery_timestamp"],true).toLocal(),
  "",
    data["payment_type"]=="CARD"?"PAID":"UNPAID",
  _total,    _format.parse(data["creation_date"],true).toLocal(),
  comments,
  data["payment_uuid"]??"",
  _deliveryCharge,
  0,0,0,"",
    customer: customerModel,
    orderId:data["order_id"],
    orderChannel:data["order_channel"],
    orderType:data["order_type"].toString().toUpperCase().replaceAll("TAKEOUT", "COLLECTION"),
    orderStatus:data["order_status"],
    discountAmount: data["discounted_amount"]??0,
    discountCode: discountCode,
    plasticBag: plasticBag==0?data["plasticBag"]??0:plasticBag,
    containerCost: containerCost==0?data["containerCost"]??0:containerCost
  );
}
  static Map<String,dynamic> toJSON(OrderModel order){

     return {
       "order_id":order.orderId,
       "order_date":DateFormat("yyyy-MM-dd HH:mm:ss").format(order.orderDateTime!.toUtc()),
       "creation_date":DateFormat("yyyy-MM-dd HH:mm:ss").format(order.orderDate!.toUtc()),
       "order_channel":order.orderChannel,
       "requested_delivery_timestamp":DateFormat("yyyy-MM-dd HH:mm:ss").format(order.requestedDeliveryTime!.toUtc()),
       "current_delivery_timestamp":DateFormat("yyyy-MM-dd HH:mm:ss").format(order.currentDeliveryTime!.toUtc()),
       "order_type":order.orderType,
       "order_status":order.orderStatus,
       "delivery_charges":order.deliveryCharge,
       "discount_code":order.discountCode??"",
       "discounted_amount":order.discountAmount,
       "payment_type":order.paymentMethod,
       "plasticBag":order.plasticBag??0,
       "containerCost":order.containerCost??0,
       "payment_uuid":order.paymentMethod=="CARD"?order.paymentUid:"-1",
       "order_items":order.items.map((e) => CartItemModel.toJSON(e)).toList(),
       "requester": CustomerModel.toJSON(order.customer!),
       "shipping_address":Address.toJSON(order.customer!.addresses[0]),
       "order_properties": {
         "comment":order.comments
       }
     };
  }
}

