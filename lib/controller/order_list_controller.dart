import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:redmango/apiService/order_service.dart';
import 'package:redmango/extra/shared_pref_manager.dart';
import 'package:redmango/model/order_model.dart';
import 'package:redmango/widgets/loader.dart';

class OrderListController extends GetxController{
  List<OrderModel> orderList = [];
  String orderType = "ALL";
  String? searchText;
  @override
  void onInit() async{
    // TODO: implement onInit
    super.onInit();
    refreshData();
  }
  deleteOldData(){

  }

  refreshData() async{
    if(searchText!=null){
      orderList = await SharedPrefManager.getOrderSearch(searchText!);
    }else{
      orderList = await SharedPrefManager.getOrderList(orderType);
    }
    update();
  }

  loadFromServer(BuildContext context)async{
    await OrderService().getAllOrder();
    refreshData();
  }
}