import 'package:get/get.dart';
import 'package:redmango/extra/shared_pref_manager.dart';
import 'package:redmango/model/font_setting.dart';

FontSettingModel? fontSettingModel;

class FontSizeController extends GetxController{

  @override
  void onInit() {
    // TODO: implement onInit
    loadFont();
    super.onInit();
  }

  void saveFontSetting(FontSettingModel model) async{
      SharedPrefManager.saveFont(model);
      fontSettingModel = model;
  }
  loadFont() async{
    fontSettingModel = await SharedPrefManager.getFontSize();
  }
}
