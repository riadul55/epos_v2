import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:redmango/controller/font_size_controller.dart';
import 'package:redmango/extra/apps_data.dart';
import 'package:redmango/pages/home_page.dart';

void main() async{
  await runZonedGuarded(() async {
    WidgetsFlutterBinding.ensureInitialized();
    await Firebase.initializeApp(
      options: const FirebaseOptions(
        apiKey: 'AIzaSyBAYpbj_yU3iHpjUWCpA874b05XZYu6EYA',
        appId: '1:724142032487:android:1bd22987e84bcfed80b6c2',
        messagingSenderId: '724142032487',
        authDomain: '724142032487-co5f6b408p61043aiqvh0ajr9b7gm9ch.apps.googleusercontent.com',
        projectId: 'storied-key-234314',
      ),
    );
    FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterError;
    runApp(MyApp());
  }, (error, stackTrace) {
    FirebaseCrashlytics.instance.recordError(error, stackTrace);
  });
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarIconBrightness: Brightness.dark,
        statusBarBrightness: Brightness.dark,
      ),
      child: MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: mainColor,
        primaryColor: mainColor,
      ).copyWith(appBarTheme: Theme.of(context)
          .appBarTheme
          .copyWith(elevation: 0, systemOverlayStyle: SystemUiOverlayStyle.light),),
      home: HomePage(),
    ));
  }
}




