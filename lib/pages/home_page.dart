import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:redmango/controller/font_size_controller.dart';
import 'package:redmango/controller/order_list_controller.dart';
import 'package:redmango/extra/apps_data.dart';
import 'package:redmango/extra/dateService.dart';
import 'package:redmango/extra/shared_pref_manager.dart';
import 'package:redmango/model/order_model.dart';
import 'package:redmango/pages/order_details.dart';
import 'package:get/get.dart';
import 'package:redmango/widgets/loader.dart';

import 'setting_page.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  OrderListController controller = Get.put(OrderListController());
  FontSizeController fontController = Get.put(FontSizeController());

  TextEditingController searchBoxController = TextEditingController();
  int currentIndex = 0;
  bool searching = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) async {
      LoaderDialog.show(context);
      if(controller.orderList.length==0){
        await controller.loadFromServer(context);
      }
      LoaderDialog.hide(context);
      updateData();
    });
  }
  updateData(){
    Timer.periodic(Duration(seconds: 60), (Timer t){
      if(mounted){
        controller.loadFromServer(context);
      }
    });
  }
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle( SystemUiOverlayStyle(
      statusBarColor: mainColor,
      statusBarBrightness: Brightness.dark,
      statusBarIconBrightness: Brightness.dark
    ));
    return GetBuilder<OrderListController>(builder: (_controller)=>Scaffold(
      appBar: AppBar(
        elevation: 0,
        leading: Icon(Icons.home),
        centerTitle: true,
        title: searching?Container(
          height: 45,
          child: TextFormField(
            controller: searchBoxController,
            textAlign: TextAlign.center,
            keyboardType: TextInputType.number,
            textInputAction: TextInputAction.done,
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 16
            ),
            cursorColor: Colors.white,
            onChanged: (str){
              controller.searchText= searchBoxController.text;
              controller.refreshData();
            },
            decoration: InputDecoration(
              hintText: "Search",
              hintStyle: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 16
              ),
              contentPadding: EdgeInsets.only(bottom: 0,top: 0,left: 15),
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(
                  color: Colors.white
                )
              ),
              enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                      color: Colors.white
                  )
              ),
            ),
          ),
        ):Text(
          "Dashboard"
        ),
        actions: [
          IconButton(
              onPressed: (){
                if(searching){
                  controller.searchText=null;
                  searching = false;
                  currentIndex = 0;
                  controller.orderType="ALL";
                  controller.refreshData();
                }else{
                  searching = true;
                }
                setState(() {

                });
              },
              icon: Icon(searching?Icons.close:Icons.search)
          ),
          IconButton(
              onPressed: () async{
                LoaderDialog.show(context);
                await _controller.loadFromServer(context);
                LoaderDialog.hide(context);
              },
              icon: Icon(Icons.refresh)
          ),
          IconButton(
              onPressed: () async{
                Navigator.push(context, MaterialPageRoute(builder: (ctx)=>SettingPage()));
              },
              icon: Icon(Icons.settings)
          )
        ],
      ),
      body: Container(
        color: Colors.deepPurpleAccent.withOpacity(.05),
        child: Column(
          children: [
            DashboardItemTitle(),
            Flexible(child: ListView(
              children: _controller.orderList.map((e) =>
                  DashboardItem(e)
              ).toList(),
            )
            )
          ],
        ),
      ),
      bottomNavigationBar:searching?null: BottomNavigationBar(
        currentIndex: currentIndex,
        onTap: (index){
          currentIndex = index;

          if(index==0){
            controller.orderType="ALL";
          }else if(index==1){
            controller.orderType="REVIEW";
          }else if(index == 2){
            controller.orderType="CLOSED";
          }
          controller.refreshData();
        },
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.list),
              label: "All",
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.search),
              label: "REVIEW"
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.close),
              label: "CLOSED"
          )
        ],
      ),
    ));
  }
}

class DashboardItem extends StatelessWidget {
  final OrderModel order;
  DashboardItem(this.order);
  @override
  Widget build(BuildContext context) {
    return
      Container(
        height: 50,
        margin: EdgeInsets.only(bottom: 2),
        decoration: BoxDecoration(
            color: order.orderStatus=="REVIEW"?Colors.deepOrange.withOpacity(.3):Colors.white,
            border: Border(

          )
        ),
        child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.resolveWith((states) => Colors.transparent
            ),
            elevation: MaterialStateProperty.resolveWith((states) =>0),
            padding: MaterialStateProperty.resolveWith((states) => EdgeInsets.only(left: 0,top: 0,bottom: 0,right: 0)),
            textStyle: MaterialStateProperty.resolveWith((states) => TextStyle(color:Colors.black)),
          ),
          onPressed: () async{
            OrderModel? _order = await SharedPrefManager.getOrderData(order.orderId);
            Navigator.push(context, animationRoute(OrderDetails(_order??order)));
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                  width: 70,
                  margin: EdgeInsets.only(right: 10),
                  alignment: Alignment.centerRight,
                  padding: EdgeInsets.only(right: 10),
                  child: Text(order.orderId.toString(),style: TextStyle(color: Colors.black),)
              ),
              Container(
                  width: 140,
                  child: Text(DateFormatHelper.fromDate(order.orderDateTime!),style: TextStyle(color: Colors.black),)
              ),
              Expanded(
                  child: Text(
                      order.orderType??"N/A",
                      textAlign: TextAlign.center,style: TextStyle(color: Colors.black),
                  )),
              Container(
                  width: 80,
                  alignment: Alignment.centerRight,
                  padding: EdgeInsets.only(right: 10),
                  child: Text(order.total.toStringAsFixed(2),style: TextStyle(color: Colors.black),)
              )
            ],
          ),
        ),
      );
  }
}
class DashboardItemTitle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 12,bottom: 12),
      decoration: BoxDecoration(
        color: Colors.deepPurpleAccent.withOpacity(.1),

      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
              width: 70,
              margin: EdgeInsets.only(right: 10),
              alignment: Alignment.centerRight,
              child: Text("Order Id".toUpperCase(),style: TextStyle(fontWeight: FontWeight.w600),)
          ),
          Container(
              width: 140,
              alignment: Alignment.center,
              child: Text("Order Date".toUpperCase(),style: TextStyle(fontWeight: FontWeight.w600))
          ),
          Expanded(
              child: Text(
                "Type".toUpperCase(),
                textAlign: TextAlign.center,
                  style: TextStyle(fontWeight: FontWeight.w600)
              )),
          Container(
              width: 80,
              alignment: Alignment.centerRight,
              padding: EdgeInsets.only(right: 10),
              child: Text("Total".toUpperCase(),style: TextStyle(fontWeight: FontWeight.w600))
          )
        ],
      ),
    );
  }
}
