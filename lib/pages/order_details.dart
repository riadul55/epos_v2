import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_sunmi_printer/flutter_sunmi_printer.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:redmango/apiService/order_service.dart';
import 'package:redmango/controller/font_size_controller.dart';
import 'package:redmango/controller/order_list_controller.dart';
import 'package:redmango/extra/apps_data.dart';
import 'package:redmango/extra/shared_pref_manager.dart';
import 'package:redmango/model/address.dart';
import 'package:redmango/model/address_properties.dart';
import 'package:redmango/model/cart_item.dart';
import 'package:redmango/model/item_component_section.dart';
import 'package:redmango/model/my_details_model.dart';
import 'package:redmango/model/order_model.dart';
import 'package:redmango/widgets/loader.dart';
import 'package:screenshot/screenshot.dart';

class OrderDetails extends StatefulWidget {
  final OrderModel model;
  OrderDetails(this.model);
  @override
  _OrderDetailsState createState() => _OrderDetailsState();
}

class _OrderDetailsState extends State<OrderDetails> {
  static const platform = const MethodChannel('printerHelper');
  int _index = 0;
  List<String> deliveryActions = ["REVIEW","PROCESSING","READY","SENT","DELIVERING","DELIVERED","CLOSED"];
  List<String> takeOutActions = ["REVIEW","PROCESSING","READY","CLOSED"];
  List<String> activeActions = ["REVIEW","PROCESSING","READY","SENT","DELIVERING","DELIVERED","CLOSED"];

  List<String> _deliveryActions = ["REVIEW","PROCESSING","READY","SENT","CLOSED"];
  List<String> _takeOutActions = ["REVIEW","PROCESSING","READY","CLOSED"];
  List<String> _activeActions = ["REVIEW","PROCESSING","READY","CLOSED"];

  OrderListController controller = Get.find();
  GlobalKey _globalPrintKey = new GlobalKey();
  ScreenshotController screenshotController = new ScreenshotController();




  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  updateOrderStatus(String toStatus) async{
    LoaderDialog.show(context);
    bool result  = await OrderService.updateStatus(widget.model,"CLOSED");
    if(result){
      widget.model.orderStatus = "CLOSED";
    }

    // for(int i = getIndex(widget.model.orderStatus);i<getIndex(toStatus);i++){
    //   String targetStatus = getNextStatus(activeActions[i]);
    //   bool result  = await OrderService.updateStatus(widget.model,targetStatus);
    //   if(result){
    //     widget.model.orderStatus = getNextStatus(targetStatus);
    //   }else{
    //     updated = false;
    //     break;
    //
    //   }
    // }
    if(result){
      await SharedPrefManager.updateOrder(widget.model);
    }else{
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Failed to update"),backgroundColor: Colors.red[900],action: SnackBarAction(onPressed: () { ScaffoldMessenger.of(context).hideCurrentSnackBar(); }, label: 'Close',textColor: Colors.white,),));
    }
    controller.refreshData();
    setState(() {

    });
    LoaderDialog.hide(context);
  }

  updateDeliveryTime() async{
    LoaderDialog.show(context);
    bool result  = await OrderService.updateStatus(widget.model,widget.model.orderStatus);
    if(result){
      await SharedPrefManager.updateOrder(widget.model);
    }else{
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Failed to update"),backgroundColor: Colors.red[900],action: SnackBarAction(onPressed: () { ScaffoldMessenger.of(context).hideCurrentSnackBar(); }, label: 'Close',textColor: Colors.white,),));

    }
    controller.refreshData();
    setState(() {

    });
    LoaderDialog.hide(context);
  }

  payNow() async{
    LoaderDialog.show(context);
    bool result  = await OrderService().payNow(widget.model.orderId,widget.model.total+widget.model.deliveryCharge-widget.model.discountAmount);
    if(result){
      widget.model.paymentStatus = "PAID";
      await SharedPrefManager.updateOrder(widget.model);
      controller.refreshData();
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Paid Success"),backgroundColor: Colors.green[900],
        action: SnackBarAction(onPressed: () { ScaffoldMessenger.of(context).hideCurrentSnackBar(); }, label: 'Close',textColor: Colors.white,),));
      setState(() {

      });
    }else{
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Failed to pay"),backgroundColor: Colors.red[900],action: SnackBarAction(onPressed: () { ScaffoldMessenger.of(context).hideCurrentSnackBar(); }, label: 'Close',textColor: Colors.white,),));
    }
    LoaderDialog.hide(context);
  }

  int getIndex(String status){
    int _index = 0;
    for(int i =0 ;i<activeActions.length;i++){
      if(status==activeActions[i]){
        _index = i;
        break;
      }
    }
    return _index;
  }





  @override
  Widget build(BuildContext context) {
    bool paid = widget.model.paymentStatus=="PAID"?true:false;
    String addressStr = "";
    if(widget.model.customer!.addresses.length!=0){
      Address address = widget.model.customer!.addresses[0];
        addressStr=address.properties.building+" "+address.properties.streetNumber+" "+address.properties.streetName+" "+address.properties.city+" "+address.properties.state+" "+address.properties.zipCode;
    }
    if(widget.model.orderType=="DELIVERY"){
      activeActions = deliveryActions;
      _activeActions = _deliveryActions;
    }else{
      activeActions = takeOutActions;
      _activeActions = _takeOutActions;
    }

    String nextStatus = getNextStatus(widget.model.orderStatus);
    return Scaffold(
      appBar: AppBar(
        title: Text("Order #"+widget.model.orderId.toString(),style: TextStyle(fontWeight: FontWeight.bold),),
        centerTitle: true,
        elevation: 0,
        brightness: Brightness.dark,
      ),
      body: Container(
        color: Colors.deepPurpleAccent.withOpacity(.05),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Expanded(
              child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Container(
                          color: Colors.white,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                height: 24,
                                margin: EdgeInsets.only(top: 10,bottom: 10,right: 10),
                                child: Image.asset(getOrderStatusImageUrl(widget.model.orderStatus),color: mainColor,),
                              ),
                              Container(
                                child: Text(widget.model.orderStatus,style: TextStyle(fontWeight: FontWeight.w700,color: Colors.black.withOpacity(.6)),),
                              ),

                            ],
                          ),
                        ),
                        Card(
                          margin: EdgeInsets.only(left: 7,right: 7,top: 7),
                          child: Column(
                            children: [
                              Container(
                                margin:EdgeInsets.only(top: 10,left: 15,right: 15,bottom: 5),
                                child: Row(
                                  children: [
                                 //   Text(widget.model.customer!.profile!.firstName!+" "+widget.model.customer!.profile!.lastName!)
                                    Expanded(
                                        child: Text(widget.model.customer!.profile!.firstName!+" "+widget.model.customer!.profile!.lastName! ,style: TextStyle(fontSize: 16,fontWeight: FontWeight.w700,color: Colors.black.withOpacity(.6)),)
                                    ),
                                    Container(
                                        width: 100,
                                        alignment: Alignment.centerRight,
                                        child: Text(DateFormat("dd/MM/yyyy").format(widget.model.orderDate!),style: TextStyle(fontSize: 16,fontWeight: FontWeight.w700,color: Colors.black.withOpacity(.5)),)
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                color: Colors.white,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(top: 5,bottom: 5,right: 10,left: 15),
                                      child: Icon(Icons.call,color: mainColor,size: 18,),
                                    ),
                                    Expanded(
                                      child: Text(widget.model.customer!.profile!.phoneNumber!,style: TextStyle(color: Colors.black.withOpacity(.7)),),
                                    ),

                                  ],
                                ),
                              ),
                              Container(
                                color: Colors.white,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(top: 5,bottom: 5,right: 10,left: 15),
                                      child: Icon(Icons.mail,color: mainColor,size: 18,),
                                    ),
                                    Expanded(
                                      child: Text(widget.model.customer!.email!,style: TextStyle(color: Colors.black.withOpacity(.7)),),
                                    ),

                                  ],
                                ),
                              ),
                              Container(
                                color: Colors.white,
                                margin: EdgeInsets.only(),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(top: 5,bottom: 5,right: 10,left: 15),
                                      child: Icon(Icons.location_on,color: mainColor,size: 18,),
                                    ),
                                    Expanded(
                                      child: Text(addressStr.trim(),style: TextStyle(color: Colors.black.withOpacity(.7)),textAlign: TextAlign.left,),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                color: Colors.white,
                                margin: EdgeInsets.only(bottom: 10,),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(top: 5,bottom: 5,right: 10,left: 15),
                                      child: Icon(Icons.local_shipping_outlined,color: mainColor,size: 18,),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(right: 20),
                                      child: Text(DateFormat("dd-MM-yyyy hh:mm a").format(widget.model.currentDeliveryTime!),style: TextStyle(color: Colors.black.withOpacity(.7)),textAlign: TextAlign.left,),
                                    ),
                                    SizedBox(
                                      height: 20,
                                      child: TextButton(
                                          onPressed: () async{
                                            print(widget.model.currentDeliveryTime!.hour);
                                            TimeOfDay? tt = await getTimeInput(TimeOfDay.fromDateTime(widget.model.currentDeliveryTime!));
                                            print(tt!.hour);
                                            String date = DateFormat("yyyy-MM-dd").format(widget.model.currentDeliveryTime!);
                                            String _dateTime = "$date ${tt.hour.toString().padLeft(2,'0')}:${tt.minute.toString().padLeft(2,'0')}:00";
                                            print(_dateTime);

                                            widget.model.currentDeliveryTime = DateTime.parse(_dateTime);
                                            updateDeliveryTime();
                                          },
                                          style: ButtonStyle(
                                            padding: MaterialStateProperty.resolveWith((states) => EdgeInsets.only(top: 0,bottom: 0))
                                          ),
                                          child: Text("Change")
                                      ),
                                    )

                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Card(
                          margin: EdgeInsets.only(left: 7,right: 7,top: 7),
                          child: Container(
                            child: ExpansionTile(
                                tilePadding: EdgeInsets.only(bottom: 0,left: 10,right: 5),
                                childrenPadding: EdgeInsets.only(bottom: 0,top: 0),
                                title: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text("Order Total",style: TextStyle(fontSize: 16,fontWeight: FontWeight.w700,color: Colors.black.withOpacity(.6))),
                                    Text("£ "+(widget.model.total-widget.model.discountAmount).toStringAsFixed(2),style: TextStyle(fontSize: 16,fontWeight: FontWeight.w700,color: Colors.black.withOpacity(.6)))
                                  ],
                                ) ,
                                children: [
                                  Column(
                                    children: widget.model.items.map((e) => CartItemView(e)).toList(),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 20,right: 10,bottom: 0,top: 5),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text("Delivery Charge",),
                                        Text("£ "+widget.model.deliveryCharge.toStringAsFixed(2),)
                                      ],
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 20,right: 10,bottom: 0,top: 5),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text("Discount "+ (widget.model.discountCode!.isEmpty ?"":" (${widget.model.discountCode})"),),
                                        Text("£ "+widget.model.discountAmount.toStringAsFixed(2),)
                                      ],
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 20,right: 10,bottom: 5,top: 5),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text("Total",style: TextStyle(fontSize: 14,fontWeight: FontWeight.w600),),
                                        Text("£ "+(widget.model.total-widget.model.discountAmount).toStringAsFixed(2),style: TextStyle(fontSize: 14,fontWeight: FontWeight.w600),)
                                      ],
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 20,right: 10,bottom: 10,top: 0),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: [
                                        Text("Order Note : ",style: TextStyle(fontSize: 14),),
                                        Text(widget.model.comments,style: TextStyle(fontSize: 14),)
                                      ],
                                    ),
                                  )
                                ],

                            ),
                          )
                        ),
                        Card(
                            margin: EdgeInsets.only(left: 7,right: 7,top: 7),
                            child: Column(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(left: 10,right: 10,top: 10,bottom: 10),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: Column(
                                          children: [
                                            Container(
                                              child: Row(
                                                children: [
                                                  Container(
                                                    child: Icon(Icons.credit_card_rounded,color: mainColor,size: 24,),
                                                    margin: EdgeInsets.only(right: 7),
                                                  ),
                                                  Text("Payment"+" ( ${widget.model.paymentMethod} )",style: TextStyle(fontSize: 16,fontWeight: FontWeight.w700,color: Colors.black.withOpacity(.6))),
                                                ],
                                              ),
                                            ),
                                          ],
                                        )
                                      ),
                                      Container(
                                        height: 35,
                                        width: 90,
                                        child: TextButton(
                                          onPressed:paid?null:() async{

                                           // payAlert();
                                          },
                                          style: ButtonStyle(
                                              backgroundColor: MaterialStateProperty.resolveWith((states) =>paid?Colors.transparent:Colors.transparent),
                                              elevation: MaterialStateProperty.resolveWith((states) => 0)
                                          ),
                                          child: Text(paid?"PAID":"Unpaid",style: TextStyle(color: paid?Colors.green[900]:Colors.red[900]),),
                                        ),
                                      )
                                    ],
                                  ),
                                ),


                              ],
                            )
                        ),
                        // Card(
                        //     margin: EdgeInsets.only(left: 7,right: 7,top: 7),
                        //     child: Container(
                        //       height: 300,
                        //       child: GridView(
                        //         gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        //             crossAxisCount: 2,
                        //             crossAxisSpacing: 5,
                        //             mainAxisSpacing: 5,
                        //             childAspectRatio: 4
                        //         ),
                        //         children: _activeActions.map((e){
                        //           return Container(
                        //             child: ElevatedButton.icon(
                        //                 onPressed:e != nextStatus?null:(){
                        //                   if(widget.model.paymentStatus!="PAID" && e=="CLOSED"){
                        //                     ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Order can't be closed without pay"),backgroundColor: Colors.red[900],action: SnackBarAction(onPressed: () { ScaffoldMessenger.of(context).hideCurrentSnackBar(); }, label: 'Close',textColor: Colors.white,),));
                        //                   }else{
                        //                     updateOrderStatus(e.replaceAll("SENT", "DELIVERED"));
                        //                   }
                        //                 },
                        //                 style: ButtonStyle(
                        //                     backgroundColor: MaterialStateProperty.resolveWith((states) => e == nextStatus?Colors.green[900]:Colors.grey.withOpacity(.3))
                        //                 ),
                        //                 icon: Image.asset(
                        //                     getOrderStatusImageUrl(e),
                        //                   height: 30,
                        //                   width: 35,
                        //                   color: e == nextStatus?Colors.white:null,
                        //                 ),
                        //                 label: Text(e)),
                        //           );
                        //         }).toList(),
                        //
                        //       ),
                        //     )),
                        
                        widget.model.orderStatus=="CLOSED"?Container():Container(
                          margin: EdgeInsets.symmetric(horizontal: 50,vertical: 15),
                          child: ElevatedButton.icon(
                              onPressed: (){
                                updateOrderStatus("CLOSED");
                              },
                              icon: Image.asset("assets/Closed_order.png",height: 25,width: 25,color: Colors.white,),
                              label: Text("CLOSE"),
                              style: ButtonStyle(
                                backgroundColor: MaterialStateProperty.resolveWith((states) => Colors.red),
                                padding: MaterialStateProperty.resolveWith((states) => EdgeInsets.symmetric(horizontal: 50))
                              ),
                          ),
                        ),


                      ],
                  )
              ),
            ),
            ElevatedButton(
                onPressed: (){
                  printMemo(widget.model);

                },
                style: ButtonStyle(
                  tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  backgroundColor: MaterialStateProperty.resolveWith((states) => mainColor)
                ),
                child: Text("Print"))
          ],
        ),
      ),
    );
  }

  Future<TimeOfDay?> getTimeInput(TimeOfDay current) async{
   return showTimePicker(
      initialTime: current,
      initialEntryMode: TimePickerEntryMode.input,
      context: context,
       builder: (BuildContext context, Widget? child) {
         return MediaQuery(
           data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: false),
           child: child!,
         );
       },
    );
  }

  payAlert(){
    showDialog(context: context, builder: (ctx){
      return AlertDialog(
        title: Text("Payment"),
        content: Text("Are you sure to pay ?"),
        actions: [
          TextButton(onPressed: (){
            Navigator.pop(context);
          }, child:Text("NO")),
          TextButton(
              onPressed: (){
                Navigator.pop(context);
                payNow();
              },
              child: Text("YES"))
        ],
      );
    });
  }

  String getNextStatus(String currentStatus){
    String str = currentStatus;
    for(int i = 0;i< activeActions.length;i++){
      if(currentStatus==activeActions[i]){
        if(i+1 < activeActions.length){
          str = activeActions[i+1];
        }else{
          str = "";
        }
      }
    }
    return str;
  }

  String getOrderStatusImageUrl(String? orderStatus){
    String assetUrl = "";
    switch(orderStatus){
      case "REVIEW":
        assetUrl = "assets/Review.png";
        break;
      case "PROCESSING":
        assetUrl = "assets/process.png";
        break;
      case "READY":
        assetUrl = "assets/Ready.png";
        break;
      case "SENT":
        assetUrl = "assets/sent.png";
        break;
      case "DELIVERING":
        assetUrl = "assets/delivering.png";
        break;
      case "DELIVERED":
        assetUrl = "assets/delivered.png";
        break;
      case "CLOSED":
        assetUrl = "assets/Closed_order.png";
        break;
      default:
        assetUrl = "assets/Active_order.png";
    }

    return assetUrl;
  }
}



class CartItemView extends StatelessWidget {
  final CartItemModel model;
  CartItemView(this.model);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 10,right: 10),
      padding: EdgeInsets.only(bottom: 5,top: 5),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            color: Colors.black.withOpacity(.25)
          )
        )
      ),
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(bottom: 5),
            child: Row(
              children: [
                Expanded(
                    child: Text(model.quantity.toString()+"X "+model.shortName,style: TextStyle(fontSize: 15,fontWeight: FontWeight.w400))
                ),
                Container(
                    width: 100,
                    alignment: Alignment.centerRight,
                    child: Text("£ "+model.total.toStringAsFixed(2),
                    )
                )
              ],
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            margin: EdgeInsets.only(left: 10),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children:model.componentSections.map((element) {
                  return Container(
                      margin: EdgeInsets.only(bottom: 3),
                      child: Text(element.sectionValue+" : "+element.selectedItem.shortName,style: TextStyle(color: Colors.black.withOpacity(.7)),));
                  }).toList() ,)),
          model.comment.length==0?Container():Container(
            alignment: Alignment.centerLeft,
            margin: EdgeInsets.only(left: 10),
            child: Text("Comments : "+model.comment,style: TextStyle(color: Colors.black.withOpacity(.7))),
          ),
        ],
      ),
    );
  }
}


printMemo(OrderModel model){
  model.items.sort((a,b){
    int aOrder = int.parse(a.printOrder.toString(),onError:(str)=> 0);
    int bOrder = int.parse(b.printOrder.toString(),onError: (str)=> 0);
    return aOrder.compareTo(bOrder);
  });
  String dotLIne = "- - - - - - - - - - - - -";

  SunmiPrinter.text("REDMANGO",styles: SunmiStyles(bold: true,align: SunmiAlign.center,size: fontSettingModel!.header1));
  SunmiPrinter.text("Indian Hitchin",styles: SunmiStyles(align: SunmiAlign.center,size: fontSettingModel!.header2));
  SunmiPrinter.text("Phone : 01462 457421",styles: SunmiStyles(align: SunmiAlign.center,size: fontSettingModel!.header2));
  SunmiPrinter.text(dotLIne,styles: SunmiStyles(align: SunmiAlign.center));
  SunmiPrinter.text(model.orderType!,styles: SunmiStyles(bold: true,align: SunmiAlign.center,size: fontSettingModel!.header1));
  SunmiPrinter.text("Order:"+DateFormat("dd/MM/yy HH:mm").format(model.orderDateTime!),styles: SunmiStyles(align: SunmiAlign.center,size: fontSettingModel!.header2));
  SunmiPrinter.text(model.orderType!+":"+DateFormat("dd/MM/yy HH:mm").format(model.currentDeliveryTime!),styles: SunmiStyles(align: SunmiAlign.center,size: fontSettingModel!.header2));
  // SunmiPrinter.text("Order No : ${widget.model.orderId}",styles: SunmiStyles(align: SunmiAlign.center));
  //  SunmiPrinter.text("Order From : ${widget.model.orderChannel}",styles: SunmiStyles(align: SunmiAlign.center));
  SunmiPrinter.text(dotLIne,styles: SunmiStyles(align: SunmiAlign.center));
  model.items.forEach((item) {

    String str3 = "";
    if(item.componentSections.length>0){
      for (ItemComponentSection section in item.componentSections){
        String _comName = section.selectedItem.shortName;
        if(_comName.toLowerCase()=="none"){
          _comName="";
        }
        // if(section.selectedItem.subComponentSelected != null){
        //   if (!section.selectedItem.subComponentSelected.shortName.equalsIgnoreCase("NONE"))
        //     _comName+=" -> "+section.selectedItem.subComponentSelected.shortName;
        // }
        str3+="${item.quantity}x${item.shortName}:$_comName";
      }
    }else{
      str3+="${item.quantity}x${item.shortName}";
    }

    SunmiPrinter.row(
      cols: [
        SunmiCol(text: str3  , width: 8,align: SunmiAlign.left),
        SunmiCol(text: '', width: 1,align: SunmiAlign.left),
        SunmiCol(text: item.total.toStringAsFixed(2), width: 3, align: SunmiAlign.right),
      ],
      bold: fontSettingModel!.itemBold,
      textSize: fontSettingModel!.header3
    );
    SunmiPrinter.text((item.comment.isEmpty?"":"Note:") + item.comment,styles: SunmiStyles(align: SunmiAlign.left,size: fontSettingModel!.header3));

  });

  SunmiPrinter.text(dotLIne,styles: SunmiStyles(align: SunmiAlign.center));
  SunmiPrinter.row(
    cols: [
      SunmiCol(text: "Subtotal"  , width: 8,align: SunmiAlign.left,),
      SunmiCol(text: '', width: 1,align: SunmiAlign.left),
      SunmiCol(text: model.subTotal!.toStringAsFixed(2), width: 3, align: SunmiAlign.right),
    ],
    textSize: fontSettingModel!.header4
  );
  SunmiPrinter.row(
    cols: [
      SunmiCol(text: "Delivery Charge"  , width: 8,align: SunmiAlign.left),
      SunmiCol(text: '', width: 1,align: SunmiAlign.left),
      SunmiCol(text: model.deliveryCharge.toStringAsFixed(2), width: 3, align: SunmiAlign.right),
    ],
      textSize: fontSettingModel!.header4
  );

  SunmiPrinter.row(
    cols: [
      SunmiCol(text: "Discount"  , width: 8,align: SunmiAlign.left),
      SunmiCol(text: '', width: 1,align: SunmiAlign.left),
      SunmiCol(text: model.discountAmount.toStringAsFixed(2), width: 3, align: SunmiAlign.right),
    ],
      textSize: fontSettingModel!.header4
  );
  SunmiPrinter.row(
    cols: [
      SunmiCol(text: "Total"  , width: 8,align: SunmiAlign.left,),
      SunmiCol(text: '', width: 1,align: SunmiAlign.left),
      SunmiCol(text: model.total.toStringAsFixed(2), width: 3, align: SunmiAlign.right),
    ],
      textSize: fontSettingModel!.header4
  );
  SunmiPrinter.text(dotLIne,styles: SunmiStyles(align: SunmiAlign.center));
  String paymentStatus = "Order not paid";
  if(model.paymentStatus == "PAID"){
    paymentStatus = "Order is Paid";
  }
  SunmiPrinter.text(paymentStatus.toUpperCase(),styles: SunmiStyles(align: SunmiAlign.center,bold: true, size: fontSettingModel!.header1));
  SunmiPrinter.text(dotLIne,styles: SunmiStyles(align: SunmiAlign.center));
  SunmiPrinter.text("Comments : " + model.comments,styles: SunmiStyles(align: SunmiAlign.left,size : fontSettingModel!.header3));

  if(model.orderType == "DELIVERY" || model.orderType == "COLLECTION"){
    if(model.customer != null){
      CustomerModel customerModel = model.customer!;
      if(customerModel.profile!= null){
        SunmiPrinter.text(customerModel.profile!.firstName!+" "+customerModel.profile!.lastName!,styles: SunmiStyles(align: SunmiAlign.left,size: fontSettingModel!.header3));
      }
      if(model.orderType != "COLLECTION"){
        if(customerModel.addresses.length>0){
          AddressProperties pro = customerModel.addresses.first.properties;

          SunmiPrinter.text(pro.building + " " + pro.streetNumber + " " + pro.streetName,styles: SunmiStyles(align: SunmiAlign.left,size: fontSettingModel!.header3));
          SunmiPrinter.text(pro.zipCode,styles: SunmiStyles(align: SunmiAlign.left,size: fontSettingModel!.header3));
          SunmiPrinter.text(pro.city,styles: SunmiStyles(align: SunmiAlign.left,size: fontSettingModel!.header3));
        }
      }
      SunmiPrinter.text(customerModel.profile!.phoneNumber!,styles: SunmiStyles(align: SunmiAlign.left,size: fontSettingModel!.header3));
    }
  }
  SunmiPrinter.emptyLines(3);

  // screenshotController.captureFromWidget(
  //   Container(
  //     child: Text("I am mobarok"),
  //   )
  //
  // ).then((value){
  //   _printMemoTest(value);
  // });
}

