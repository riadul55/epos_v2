import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:redmango/controller/font_size_controller.dart';
import 'package:redmango/extra/shared_pref_manager.dart';
import 'package:redmango/model/font_setting.dart';

class SettingPage extends StatefulWidget {
  @override
  State<SettingPage> createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {
  final TextEditingController txtHeader1Controller = TextEditingController();

  final TextEditingController txtHeader2Controller = TextEditingController();

  final TextEditingController txtHeader3Controller = TextEditingController();

  final TextEditingController txtHeader4Controller = TextEditingController();

  final FontSizeController fontSizeController = Get.find();
  final formKey = GlobalKey<FormState>();
  dynamic itemBold = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    refreshBox();
  }
  void refreshBox() {
    txtHeader1Controller.text = fontSettingModel!.header1.toString();
    txtHeader2Controller.text = fontSettingModel!.header2.toString();
    txtHeader3Controller.text = fontSettingModel!.header3.toString();
    txtHeader4Controller.text = fontSettingModel!.header4.toString();
    itemBold = fontSettingModel!.itemBold;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Setting"),
      ),
      body: SingleChildScrollView(
        child: Form(
          key: formKey,
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(left: 10,right: 10,top: 10),
                child: TextFormField(
                  controller: txtHeader1Controller,
                  decoration: InputDecoration(
                      hintText: "Header 1",
                      labelText: "Header 1",
                      border: OutlineInputBorder(

                      )
                  ),
                  keyboardType: TextInputType.number,
                  validator: (str){
                    return str!.isEmpty?"required":null;
                  },
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 10,right: 10,top: 10),
                child: TextFormField(
                  controller: txtHeader2Controller,
                  decoration: InputDecoration(
                      hintText: "Header 2",
                      labelText: "Header 2",
                      border: OutlineInputBorder(

                      )
                  ),
                  keyboardType: TextInputType.number,
                  validator: (str){
                    return str!.isEmpty?"required":null;
                  },
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 10,right: 10,top: 10),
                child: TextFormField(
                  controller: txtHeader3Controller,
                  decoration: InputDecoration(
                      hintText: "Header 3",
                      labelText: "Header 3",
                      border: OutlineInputBorder(

                      )
                  ),
                  keyboardType: TextInputType.number,
                  validator: (str){
                    return str!.isEmpty?"required":null;
                  },
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 10,right: 10,top: 10),
                child: TextFormField(
                  controller: txtHeader4Controller,
                  decoration: InputDecoration(
                      hintText: "Header 4",
                      labelText: "Header 4",
                      border: OutlineInputBorder(

                      )
                  ),
                  keyboardType: TextInputType.number,
                  validator: (str){
                    return str!.isEmpty?"required":null;
                  },

                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 10,right: 10,top: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Flexible(
                        child: Text("Item Bold",
                          style: TextStyle(
                            fontWeight: FontWeight.w700,
                            fontSize: 16
                          ),
                        )
                    ),
                    
                    Container(
                      width: 120,
                      child: RadioListTile(
                        groupValue: itemBold,
                        onChanged: (value) {
                          itemBold = value;
                          setState(() {

                          });
                        },
                        value: false,
                        title: Text("No"),
                      ),
                    ),
                    Container(
                      width: 120,
                      child: RadioListTile(
                        groupValue: itemBold,
                        onChanged: (value) {
                          itemBold = value;
                          setState(() {

                          });
                        },
                        value: true,
                        title: Text("Yes"),
                      ),
                    )
                  ],
                )
              ),
              Container(
                child: ElevatedButton(
                  onPressed: () {
                    if(formKey.currentState!.validate()){
                      fontSizeController.saveFontSetting(
                          FontSettingModel(
                              header1: int.tryParse(txtHeader1Controller.text)??0,
                              header2: int.tryParse(txtHeader2Controller.text)??0,
                              header3: int.tryParse(txtHeader3Controller.text)??0,
                              header4: int.tryParse(txtHeader4Controller.text)??0,
                              itemBold: itemBold
                          )
                      );
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Successfully Saved"), backgroundColor: Colors.green[900],duration: Duration(seconds: 1),));
                    }
                  },
                  child: Text("SAVE"),
                  style: ButtonStyle(
                      padding: MaterialStateProperty.resolveWith((states) => EdgeInsets.symmetric(horizontal: 20))
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
