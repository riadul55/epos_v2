import 'dart:convert';
import 'dart:io';

class ApiClient{

    // final baseURL = "https://labapi.yuma-technology.co.uk:8443";
    // final String sessionToken = "0b09064c-82e1-4548-b693-b6081df85b39";
    // final String providerID = "a7033019-3dee-457b-a9da-50df238b3c9f";

  //hitchin
  final baseURL = "https://priapi.yuma-technology.co.uk:9014";
  final String sessionToken = "fe98f766-7415-4707-bcd6-9a6853fc808f";
  final String providerID = "d5ab4efc-b8d9-11eb-9e8f-b63f3ad1ecdd";

  // Bukara
  //   final baseURL = "https://priapi.yuma-technology.co.uk:9020";
  //   final String sessionToken = "d5ab4efc-b8d9-11eb-9e8f-b63f3ad1ecd2";
  //   final String providerID = "f1f8f459-1ea6-402d-ab2f-36b7ee8b8524";




  static ApiClient getInstance = ApiClient();
  HttpClient getHttpClient() {
    HttpClient webClient = HttpClient();
    webClient.connectionTimeout = Duration(seconds: 15);
    webClient.idleTimeout = Duration(seconds: 50);
    webClient.userAgent ="RedMangoPOS";
    webClient.badCertificateCallback =
    ((X509Certificate cert, String host, int port) => true);
    return webClient;
  }
  post(formData, urlPath) async {
    try {
      HttpClientRequest request =
      await getHttpClient().postUrl(Uri.parse(baseURL + urlPath));
      request.headers.set('content-type', 'application/json');
      request.headers.set('Accept', 'application/json');
      request.headers.set('ProviderSession', sessionToken);
      request.add(utf8.encode(formData));
      return await request.close();
    } catch (e) {
      return null;
    }
  }

  get(urlPath) async {
    print(baseURL + urlPath);
    try {
      HttpClientRequest request = await getHttpClient().getUrl(Uri.parse(baseURL + urlPath));
      request.headers.set('content-type', 'application/json');
      request.headers.set('ProviderSession', sessionToken);
      return await request.close();
    } catch (e) {
      print(e.toString());
      return null;
    }
  }
  postWithoutBody(urlPath) async {
    try {
      HttpClientRequest request =
      await getHttpClient().postUrl(Uri.parse(baseURL + urlPath));
      request.headers.set('content-type', 'application/json');
      request.headers.set('Accept', 'application/json');
      request.headers.set('ProviderSession', sessionToken);
      return await request.close();
    } catch (e) {
      return null;
    }
  }
  getPublic(urlPath) async {
    try {
      HttpClientRequest request = await getHttpClient().getUrl(Uri.parse(baseURL + urlPath));
      request.headers.set('content-type', 'application/json');
      return await request.close();
    } catch (e) {
      print(e.toString());
      return null;
    }
  }
  patch(formData,urlPath) async {
    try {
      HttpClientRequest request =
      await getHttpClient().patchUrl(Uri.parse(baseURL + urlPath));
      request.headers.set('content-type', 'application/json');
      request.headers.set('ProviderSession', sessionToken);
      request.add(utf8.encode(formData));
      return await request.close();
    } catch (e) {
      print(e.toString());
      return null;
    }
  }
  delete(urlPath) async {
    try {
      HttpClientRequest request =
      await getHttpClient().deleteUrl(Uri.parse(baseURL + urlPath));
      request.headers.set('content-type', 'application/json');
      request.headers.set('ProviderSession', sessionToken);
      return await request.close();
    } catch (e) {
      print(e.toString());
      return null;
    }
  }
}