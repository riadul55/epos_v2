import 'dart:convert';
import 'dart:io';
import 'package:redmango/extra/shared_pref_manager.dart';
import 'package:redmango/model/order_model.dart';
import 'package:redmango/widgets/loader.dart';
import 'api_client.dart';
import 'package:get/get.dart';
class OrderService{
  Future getAllOrder() async {
    HttpClientResponse? response = await ApiClient.getInstance.get("/delivery/order?status=REVIEW");
    bool playingSound = false;
    // ignore: unnecessary_null_comparison
    if (response==null) {
      return [];
    }else {
      print(response.statusCode);
      if (response.statusCode != 200) {
        return [];
      }
      String body = await response.transform(utf8.decoder).join();
      dynamic responseList = jsonDecode(body);
      for(var element in responseList) {
        OrderModel item = OrderModel.fromJSON(element);
        if(! await SharedPrefManager.orderExist(item.orderId)){
          item = await getOrderDetails(item.orderId);
          await SharedPrefManager.addToOrderList(item,!playingSound);
          playingSound = true;
        }
      }
    }
  }
  Future<dynamic> getOrderDetails(int? orderId) async {
    HttpClientResponse? response = await ApiClient.getInstance.get("/delivery/order/$orderId");
    // ignore: unnecessary_null_comparison
    if (response==null) {
      return null;
    }else {
      print(response.statusCode);
      if (response.statusCode != 200) {
        return null;
      }
      String body = await response.transform(utf8.decoder).join();
      dynamic data = jsonDecode(body);
      OrderModel item = OrderModel.fromJSON(data);
      return item;
    }
  }
  static Future<bool> updateStatus(OrderModel model,String status) async {
    var formData = {
      "order_id":model.orderId,
      "order_type":model.orderType,
      "order_status":status,
      "current_delivery_timestamp":model.currentDeliveryTime.toString(),
      "driver_provider_uuid": ApiClient.getInstance.providerID
    };
    print(formData);
    HttpClientResponse? response = await ApiClient.getInstance.patch(jsonEncode(formData),"/delivery/order/${model.orderId}");
    // ignore: unnecessary_null_comparison
    if (response==null) {
      return false;
    }else {
      if (response.statusCode != 202) {
        return false;
      }
      return true;
    }
  }

  Future<bool> payNow(int orderID,double amount) async {
    var formData = {
      "entry_type":"CASH_ORDER",
      "amount":amount,
      "comment":"Cash Payment",
      "provider_uuid": ApiClient.getInstance.providerID
    };
    HttpClientResponse? response = await ApiClient.getInstance.post(jsonEncode(formData),"/delivery/order/$orderID/cash_entry");
    // ignore: unnecessary_null_comparison
    if (response==null) {
      return false;
    }else {
      print(response.statusCode);
      if (response.statusCode != 201) {
        if(response.statusCode==409){
          return true;
        }
        String body = await response.transform(utf8.decoder).join();
        print(body);
        return false;
      }
      return true;
    }
  }

}