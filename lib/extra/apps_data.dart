import 'package:flutter/material.dart';
import 'package:get/get.dart';

dynamic mainColor = Colors.deepOrange;
showAlertDialog(msg,{VoidCallback? retry}) {
  Get.defaultDialog(
    content: Text(msg),
    actions: <Widget>[
      if (retry==null) TextButton(
        child: Text("Try Again"),
        onPressed: () {
          Get.back();
          retry!();
        },
      ) else Container(),
      TextButton(
        child: Text("Ok"),
        onPressed: () {
          Get.back();
        },
      )
    ],
  );
}
Route animationRoute(destinationPage) {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => destinationPage,
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      var begin = Offset(1, 0);
      var end = Offset.zero;
      var curve = Curves.ease;

      var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

      return SlideTransition(
        position: animation.drive(tween),
        child: child,
      );
    },
  );
}