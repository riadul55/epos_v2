import 'dart:io';
import 'dart:typed_data';

import 'package:intl/intl.dart';

class DateFormatHelper{

  static String fromString(String str){

      try{
        DateTime f = DateTime.parse(str);
        return DateFormat('dd-MM-yyyy').format(f);
      }catch(e){
        return str;
      }
  }
  static String fromDate(DateTime dateTime){
      return DateFormat('dd-MM-yyyy hh:mm a').format(dateTime);
  }

  static DateTime parseDate(String str) {
    try{
      return DateTime.parse(str);
    }catch(e){
      return DateTime.parse("2000-01-01");
    }

  }
}