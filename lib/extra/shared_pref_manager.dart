import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:flutter_ringtone_player/flutter_ringtone_player.dart';
import 'package:intl/intl.dart';
import 'package:redmango/model/font_setting.dart';
import 'package:redmango/model/order_model.dart';
import 'package:redmango/pages/order_details.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefManager {

  static Future<OrderModel?> getOrderData(int orderId) async {
    SharedPreferences myData = await SharedPreferences.getInstance();
    OrderModel? _myOrder;
    List<String> orderStr = myData.getStringList("orderList")??[];
    for(String element in orderStr){
      OrderModel _order = OrderModel.fromJSON(jsonDecode(element));
      if(_order.orderId==orderId){
        _myOrder = _order;
      }
    }
    return _myOrder;
  }

  static Future<List<OrderModel>> getOrderList(String status) async {
    SharedPreferences myData = await SharedPreferences.getInstance();
    List<OrderModel> orderList = [];
    List<String> orderStr = myData.getStringList("orderList")??[];
    for(String element in orderStr){
      OrderModel _order = OrderModel.fromJSON(jsonDecode(element));
      String _orderDateStr = DateFormat("yyyyMMdd").format(_order.orderDate!);
      String _todayDateStr = DateFormat("yyyyMMdd").format(DateTime.now());

      print(_todayDateStr);
      print(_orderDateStr);

      if(_orderDateStr == _todayDateStr){
        if(_order.orderStatus==status){
          orderList.add(_order);
        }else if(status=="ALL"){
          orderList.add(_order);
        }
      }


    }
    //
    // orderStr = [];
    // orderList.forEach((element) {
    //   orderStr.add(jsonEncode(OrderModel.toJSON(element)));
    // });
    // myData.setStringList("orderList", orderStr);

    orderList.sort((b,a)=>a.orderId.compareTo(b.orderId));

    return orderList;
  }
  static Future<List<OrderModel>> getOrderSearch(String orderId) async {
    SharedPreferences myData = await SharedPreferences.getInstance();
    List<OrderModel> orderList = [];
    List<String> orderStr = myData.getStringList("orderList")??[];
    for(String element in orderStr){
      OrderModel _order = OrderModel.fromJSON(jsonDecode(element));
      if(_order.orderId.toString().contains(orderId)){
        orderList.add(_order);
      }
    }
    orderList.sort((b,a)=>a.orderId.compareTo(b.orderId));
    return orderList;
  }

  static addToOrderList(OrderModel model,bool sound) async {
    SharedPreferences myData = await SharedPreferences.getInstance();
    List<OrderModel> orderList = [];
    List<String> orderStr = myData.getStringList("orderList")??[];
    orderStr.forEach((element) {
      orderList.add(OrderModel.fromJSON(jsonDecode(element)));
    });

    bool exist = false;
    orderList.forEach((element) {
      if(element.orderId==model.orderId){
        exist = true;
      }
    });
    if(!exist){
      orderStr.add(jsonEncode(OrderModel.toJSON(model)));
      try{
        printMemo(model);
      }catch(e){
        print(e);
      }
      if(sound)
        playSound();
    }
    await myData.setStringList("orderList", orderStr);
  }

  static updateOrder(OrderModel model) async {
    SharedPreferences myData = await SharedPreferences.getInstance();
    List<OrderModel> orderList = [];
    List<String> orderStr = myData.getStringList("orderList")??[];
    orderStr.forEach((element) {
      OrderModel _model = OrderModel.fromJSON(jsonDecode(element));
      if(_model.orderId== model.orderId){
        _model = model;
      }
      orderList.add(_model);
    });
    orderStr = [];
    orderList.forEach((element) {
      orderStr.add(jsonEncode(OrderModel.toJSON(element)));
    });
    await myData.setStringList("orderList", orderStr);
  }

  static orderExist(int orderId) async {
    SharedPreferences myData = await SharedPreferences.getInstance();
    List<OrderModel> orderList = [];
    List<String> orderStr = myData.getStringList("orderList")??[];
    orderStr.forEach((element) {
      orderList.add(OrderModel.fromJSON(jsonDecode(element)));
    });
    bool exist = false;
    orderList.forEach((element) {
      if(element.orderId==orderId){
        exist = true;
      }
    });
    return exist;
  }
  static playSound() async{
    FlutterRingtonePlayer.playNotification();
  }

  static void saveFont(FontSettingModel model) async {
    SharedPreferences myData = await SharedPreferences.getInstance();
    await myData.setString("fontSize", jsonEncode(model.toJSON()));
  }
  static Future<FontSettingModel> getFontSize() async {
    SharedPreferences myData = await SharedPreferences.getInstance();
    var data = myData.getString("fontSize")??"";
    if(data.isEmpty){
      return Future.value(FontSettingModel(
        header1: 35,
        header2: 30,
        header3: 25,
        header4: 20,
        itemBold: false
      ));
    }
    return Future.value(FontSettingModel.fromJSON(jsonDecode(data)));
  }

}
